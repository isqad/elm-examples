module SignIn exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onSubmit)


main =
    Html.beginnerProgram { view = view, model = model, update = update }


minPasswordLength : Int
minPasswordLength =
    8


type alias Model =
    { email : String
    , password : String
    , passwordConfirm : String
    , errors : String
    }


model : Model
model =
    Model "" "" "" ""


type Msg
    = Email String
    | Password String
    | PasswordConfirm String
    | Form


update : Msg -> Model -> Model
update msg model =
    case msg of
        Email email ->
            { model | email = email }

        Password pwd ->
            { model | password = pwd }

        PasswordConfirm pwd ->
            { model | passwordConfirm = pwd }

        Form ->
            if model.password == model.passwordConfirm then
                if String.length model.password < minPasswordLength then
                    { model | errors = "Min password length should be greater than " ++ toString minPasswordLength ++ " symbols" }
                else
                    { model | errors = "" }
            else
                { model | errors = "Password do not match" }


view : Model -> Html Msg
view model =
    div []
        [ Html.form [ onSubmit Form ]
            [ div [ class "form-group" ]
                [ label [ for "sign-in-email" ] [ text "Email:" ]
                , input [ type_ "email", placeholder "name@example.com", id "sign-in-email", class "form-control", onInput Email ] []
                ]
            , div [ class "form-group" ]
                [ label [ for "sign-in-password" ] [ text "Password:" ]
                , input [ type_ "password", placeholder "Password", id "sign-in-password", class "form-control", onInput Password ] []
                ]
            , div [ class "form-group" ]
                [ label [ for "sign-in-password-confirm" ] [ text "Confirm your password:" ]
                , input [ type_ "password", placeholder "Confirm your password", id "sign-in-password-confirm", class "form-control", onInput PasswordConfirm ] []
                ]
            , input [ type_ "submit", class "btn btn-primary", value "Sign up" ] []
            , div [] [ text model.errors ]
            ]
        ]
