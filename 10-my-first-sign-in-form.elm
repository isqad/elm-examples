module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onSubmit)


main =
    Html.beginnerProgram { view = view, model = model, update = update }


minPasswordLength : Int
minPasswordLength =
    8


type alias Model =
    { email : String
    , password : String
    , passwordConfirm : String
    , errors : String
    }


model : Model
model =
    Model "" "" "" ""


type Msg
    = Email String
    | Password String
    | PasswordConfirm String
    | Form


update : Msg -> Model -> Model
update msg model =
    case msg of
        Email email ->
            { model | email = email }

        Password pwd ->
            { model | password = pwd }

        PasswordConfirm pwd ->
            { model | passwordConfirm = pwd }

        Form ->
            if model.password == model.passwordConfirm then
                if String.length model.password < minPasswordLength then
                    { model | errors = "Min password length should be greater than " ++ toString minPasswordLength ++ " symbols" }
                else
                    { model | errors = "" }
            else
                { model | errors = "Password do not match" }


view : Model -> Html Msg
view model =
    div []
        [ Html.form [ onSubmit Form ]
            [ input [ type_ "email", placeholder "email", onInput Email ] []
            , input [ type_ "password", placeholder "password", onInput Password ] []
            , input [ type_ "password", placeholder "confirm your password", onInput PasswordConfirm ] []
            , input [ type_ "submit", value "Sign in" ] []
            , div [] [ text model.errors ]
            ]
        ]
